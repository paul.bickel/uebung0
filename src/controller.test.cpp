#include <catch2/catch_test_macros.hpp>
extern "C" {
#include "controller.h"
}

TEST_CASE("PI_Regler") {
    REQUIRE(controller(8, 0, 500) == 16);
    REQUIRE(controller(-2, 0, 500) == -4);
    REQUIRE(controller(0, 2, 500) == -4);
}

TEST_CASE("limits") {
    REQUIRE(controller(2, 0, 2) == 2);
    REQUIRE(controller(-2, 0, 2) == -2);
}