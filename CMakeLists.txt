cmake_minimum_required(VERSION 3.26)
project(Uebung0)

set(CMAKE_C_STANDARD 11)
set(CMAKE_C_STANDARD_REQUIRED ON)
set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_library(common OBJECT
        src/controller.c
        src/controller.h)

add_executable(Uebung0 src/main.c
        src/controller.c
        src/controller.h)

# Fetch Catch2
Include(FetchContent)
FetchContent_Declare(
        Catch2
        GIT_REPOSITORY https://github.com/catchorg/Catch2.git
        GIT_TAG        v3.3.2
)
FetchContent_MakeAvailable(Catch2)

# Build Tests
enable_testing()


add_executable(test_controller
        $<TARGET_OBJECTS:common>
        src/controller.test.cpp
)

target_link_libraries(test_controller PRIVATE Catch2::Catch2WithMain)
target_include_directories(test_controller PRIVATE src/)
add_test(test_controller env CTEST_OUTPUT_ON_FAILURE=1 ./test_controller)

